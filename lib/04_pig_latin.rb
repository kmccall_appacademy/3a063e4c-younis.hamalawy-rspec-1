def translate(sentence)
  translated_sentence = []
  sentence.split.each do |word|
    translated_sentence << piggy(word)
  end
  translated_sentence.join(' ')
end

def piggy(word)
  punct = "-[];',./!@#%&*()_{}::?"
  punctuation = ""
  word.each_char { |ch| punctuation << ch if punct.include?(ch) }
  vowels = "aeiou"
  mod_word = word.downcase.delete(punct)
  until vowels.include?(mod_word[0])
    if mod_word[0] == "q"
      mod_word = mod_word[2..-1] + mod_word[0..1]
    else
      mod_word = mod_word[1..-1] + mod_word[0]
    end
  end
  if word[0] != word[0].downcase
    mod_word = mod_word.capitalize
  end
    mod_word + "ay" + punctuation
end
